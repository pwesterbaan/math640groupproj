import sys
import os
import Gnuplot
import Numeric
import math
from numpy import *
from numpy.linalg import *
#from numpy.oldnumeric.linear_algebra import *
#from Numeric import *

""" 
Data Creating Script                                               John Chrispell
                                                                        1/29/2009
    This is a python script to create some function data that can be used to test 
    the "LeastSquareSwimmer.py" script. 
"""

# Options
nsets        = 10           # Total Number of data sets to output to file.
npts         = 321          # Number of points to place in each data set.
timestep     = 0.1          # The time step to place in between data sets.
Length       = 12.0         # Length of fake swimmer to create. 
PoltsOn      = 1            # (0 or 1) 1 Turns on the plotting utility.
SavePlots    = 1            # (0 or 1) 1 Saves plots of each data set to a file.
showonscreen = 0            # (0 or 1) 1 Shows the plot in an x window.
filename     = "ourdata.dat"   # Name of the file to save the data sets to. 
swimupdown   = 0            # (0 or 1) 0 gives left right, 1 gives up down

# Parameters for function y = (ax^2 + bx + c)sin(kx + omega*t + phi)
a     = 0.5
b     = 0.75
c     = 1.5
k     = 0.75
omega = -7.0

# =============================================================================
# YOU SHOULDN'T NEED TO EDIT BELOW THIS LINE
# =============================================================================
#        Lampray Data Params Found: a =  0.00139457095468 
#                                   b =  0.0161444381857  
#                                   c =  0.00358079651072  
#                                   k =  0.752712459267  
#                                 phi =  1.24932868114
# =============================================================================


xpoints      = zeros(npts, float); xpoints.shape=(npts,1)
ypoints      = zeros(npts, float); ypoints.shape=(npts,1)


FILE = open(filename,"w")

for j in range(nsets):
    t = 0.0 + timestep*(j+1)
    for i in range(npts):
        x = 0.0 + float((i/float(npts)))*Length
        if(swimupdown == 0):
            xpoints[i,0] = x
            ypoints[i,0] = (a*x*x + b*x + c)*sin(k*x + t*omega)
        else:
            xpoints[i,0] = (a*x*x + b*x + c)*sin(k*x + t*omega)
            ypoints[i,0] = x
        FILE.write(str(xpoints[i,0])) 
        FILE.write(" ")
        FILE.write(str(ypoints[i,0])) 
        FILE.write("\n")

    if(PoltsOn == 1):
        SavePlotName = ''
        if(SavePlots == 1):
            SavePlotName = 'plot'+ str(timestep*(j+1)) + '.eps' 
        g  = Gnuplot.Gnuplot(debug=2)
        d = Gnuplot.Data(xpoints[:,0],ypoints[:,0])
        Title = 'Function at time '+ str(t)
        xaxis = 'x axis'
        yaxis = 'y axis' 
        g.title(Title)     
        g('set data style lines') 
        g('set size square')
        g.xlabel(xaxis)
        g.ylabel(yaxis)
        if(showonscreen == 1):
            g('set terminal x11')
            g.plot(d)
            raw_input('Please press return to continue...\n')

        if(SavePlotName != ''):
            outFile = 'set output \"' + SavePlotName + '\"'
            g(outFile)
            g('set terminal postscript')
            g.plot(d)

FILE.close() 
