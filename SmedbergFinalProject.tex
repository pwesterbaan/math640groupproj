\documentclass{article}
\usepackage[utf8]{inputenc}

\title{finalproject}
\author{Suzanne Smedberg}
\date{December 2015}

\begin{document}

\maketitle
\section{Introduction}
Modeling biological motion presents a complex problem.  Even a relatively simple organism, like a eukaryotic flagellar swimmer in a fluid, presents a unique set of challenges \cite{vog080}.  The simple swimmer occurs frequently in nature.  A drop of water from a pond or stream can contain hundreds of self-propellent, undulation organisms.  While macro organisms like the tuna can swim approximately 10 body lengths a second, their microscopic counterparts can move with relative speeds of an order of magnitude larger \cite{yat860}.  Sperm swims to its biological imperative by a flagellum \cite{smi090}.  Larger animals can control the undulating motion in both homogeneous fluids \cite{jor960} and those of varying densities \cite{doo120} to achieve different locomotive goals.  While so different in structure, relative speed, and biological outcome, all of these swimming bodies follow the same simple swimmer model.  By simplifying this model, and applying to both Newtonian and complex fluids, much can be inferred about a broad range of biological situations.  Perhaps the best model of simple swimmer, the \emph{Caenorhabditis elegans} nematode, is an excellent model with which to begin \cite{fen010}. In her research at Tulane University, Chia Yu created a simulation to model the fluid structure interaction of the Lamprey, a simple swimmer model \cite{tyt010}.  Using her results, we can fit a non-linear model to the given swimmer, further predicting its motion at a later timestep. \\

The goal is to fit the swimmer to the mathematical model of the form:
$$ f\left(x,\hat{t}\right) = \left(ax^2 + bx + c\right) \sin{\left(kx + \omega t\right)} $$
where non-linear data fitting will produce the values of $a$, $b$, $c$, $k$, and $\omega$ that allow for the best linear fit.  We will use the Gauss-Newton Method of non-linear data fitting to implement an algorithm to find the best fit for our given swimmer, reading in the data to determine position $x$ at given time $t$.  We will then use that data to model the swimmer.  Finally, we will consider any further applications of the model. \\

\section{Background}
We begin with Newton's method.  Consider a function $f: \mathbf{R}^n \rightarrow \mathbf{R}^n $, with a truncated Taylor series expansion given by:
$$ \mathbf{f}\left(\mathbf{x} + \mathbf{s}\right) \approx \mathbf{f}\left(\mathbf{x}\right) + \mathbf{J_{f}}\left(\mathbf{x}\right)\mathbf{s}$$ 
where $\mathbf{J_{f}}\left(\mathbf{x}\right)$ denotes the Jacobian matrix of $\mathbf{x}$.  The Jacobian is defined by 
$$\mathbf{J_{f}}\left(\mathbf{x}\right)_{i,j} = \frac{\partial f_i\left(\mathbf{x}\right)}{\partial x_j}. $$
The value of the spacial discretization step $\mathbf{s}$ is defined as:
$$ \mathbf{x}_n = \mathbf{s} + \mathbf{x} $$
Since $\mathbf{x}_n$ is an approximation to zero (the overall goal of the algorithm) for $\mathbf{f}$, the equation above can be written as:
$$ \mathbf{x}_n \approx \mathbf{x} - \frac{\mathbf{f}(\mathbf{x})}{\mathbf{J_{f}}\left(\mathbf{x}\right)} $$
By simplifying then, we can solve for $\mathbf{s}_n$ such as:
$$ \mathbf{s}_n = \frac{-\mathbf{f}\left(\mathbf{x}_{n-1}\right)}{\mathbf{J}_f \left(\mathbf{x}_{n-1}\right)} $$
But this algorithm only considers functions within the same dimension. We can generalize Newton's algorithm, then   Considering the function $f: \mathbf{R}^m \rightarrow \mathbf{R}$, a modification must be made.  The new, two term, Taylor expansion can be written then as:
$$ f\left(\mathbf{x} + \mathbf{s}\right) \approx f\left(\mathbf{x}\right) + \nabla f\left(\mathbf{x}\right)^{T}\mathbf{s} + \frac{1}{2}\mathbf{s}^{T}\mathbf{H}_f\left(\mathbf{x}\right)\mathbf{s}. $$
where $\mathbf{H}_f\left(\mathbf{x}\right)$ is the Hessian matrix of second partial derivatives of $f$ such that:
$$ \mathbf{H}_f\left(\mathbf{x}\right)_{i,j} = \frac{\partial^2 f(\mathbf{x})}{\partial x_i \partial x_j} $$
thus minimizing $f(x+s)$ when 
$$ \mathbf{H}_f\left(\mathbf{x}\right)\mathbf{s} = -\nabla f(\mathbf{x}) $$
In practice, then, the goal is to minimize the residual $\mathbf{r}$ where $\mathbf{r}:\mathbf{R}^k \rightarrow \mathbf{R}^m$ such that:
$$ r_i(\mathbf{x}) = y_i - f\left(t_i,\mathbf{x}\right), \; \mbox{ for } \; i\in \{1,2,...,m\}. $$
where the goal is to minimize
$$\phi(\mathbf{x}) = \frac{1}{2} \|\mathbf{r}\|_{2}^{2} = \frac{1}{2}\mathbf{r}(\mathbf{x})^{T}\mathbf{r}(\mathbf{x}). $$
This is considered the method of least squares because we are minimizing the squares of the residuals, and thus the vertical distance between the actual value and the approximation are minimized.  We can apply Newton's method by seperating the following components:
\begin{eqnarray}
\nabla\phi(\mathbf{x}) & = & \mathbf{J}^{T}(\mathbf{x})\mathbf{r}(\mathbf{x})  \\
\mathbf{H}_{\phi}(\mathbf{x}) & = & \mathbf{J}^{T}(\mathbf{x})\mathbf{J}(\mathbf{x}) + \Sigma_{i=1}^{m}r_i(\mathbf{x})\mathbf{H}_{r_i}(\mathbf{x})
\end{eqnarray}
Calculating the $\mathbf{H}_{r_i}(\mathbf{x})$ term is computational difficult, so it is usually ignored.  This leads to solving instead
$$ \mathbf{s}_n = \frac{-\mathbf{J}^{T}\left(\mathbf{x}_{n-1}\right)\mathbf{r}\left(\mathbf{x}_{n-1}\right)}{\mathbf{J}^{T}\left(\mathbf{x}_{n-1}\right)\mathbf{J}\left(\mathbf{x}_{n-1}\right)} $$
and then updating $\mathbf{x}_n = \mathbf{x}_{n-1} + \mathbf{s}_n$.
\section{Swimmer Algorithm}
So, then specifically for our swimmer, we have made a few assumptions:
\begin{enumerate}
\item We assume the swimmer is oriented in such a way that his motion is only in the positive $x$-direction.  An actual swimmer does not only swim in a straight line, but the considering the two-dimension motion adds a high level of complexity.
\item We assume that all the motion is relative only to time and horizontal position, and follows the same model.  
\item We assume that the dependence on time is linearly related to a variable $\phi$
\end{enumerate}
Reconsider the swimmer model:
$$ f\left(\mathbf{x},\hat{t}\right) = \left(ax^2 + bx + c\right) \sin{\left(kx + \omega t\right)}. $$
To simplify the non-linear dependence of the sine function, we can consider a variable $\phi$ such that $\phi = \omega t$.  By replacement, then
$$ f\left(\mathbf{x},\hat{t}\right) = \left(ax^2 + bx + c\right) \sin{\left(kx + \phi\right)}, $$
for a vector $\hat{\mathbf{x}} = \left(a,b,c,k,\phi\right)^{T}$
This vector can be solved using the Normal equations for non-linear least squares.  Recall that the normal equations are in the form
$$A^TAx=A^Tb,$$ where the $A$ matrix is over-determined using data from the function.  We are using a modified version of this form with the Jacobian such that
$$ \mathbf{J}^T\left(x_{n-1}\right)\mathbf{J}\left(x_{n-1}\right)\mathbf{s}_n = -\mathbf{J}^T\left(x_{n-1}\right)\mathbf{r}\left(x_{n-1}\right)$$
At each iteration, we are considering the residual (because the goal of least squares is to minimize the residual function) as 
$$r_i\left(\hat{\mathbf{x}}\right) = y_i - f\left(\hat{\mathbf{x}},t_i\right) \; \mbox{for} \; i\in \{1,2,...,m\}$$
Consider then, the following Jacobian terms:
\begin{eqnarray*}
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,1} & = & -t_i^2 \sin{\left(kt_i + \phi\right)} \\
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,2}  & = & -t_i \sin{\left(kt_i + \phi\right)} \\
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,3} & = & - \sin{\left(kt_i + \phi\right)} \\
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,4} & = & -k\left(at_i^2 + bt_i + c\right) \cos\left(kt_i + \phi\right) \\
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,5} & = & -\left (at_i^2 + bt_i + c\right) \cos{\left(kt_i + \phi\right)} 
\end{eqnarray*}
This creates a $5x5$ matrix system on which we can iterate the points such that each $x_{new} = x_{old} + s_n $, where $s$ is the solution to the Jacobian matrix system.
This iterative system returns the values of $a$, $b$, $c$, $k$, and $\phi$, but provides no value for the term $\omega$.  Recall that $\phi = \omega t$, so for many values of t, the relationship between $\phi$ and $\omega$ can be determined by finding a linear fit of $\phi$ v. $t$.  The slope of that line is the value of $\omega$.  
\end{document}
