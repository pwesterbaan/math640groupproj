# Define standard library imports.
from math import*
import numpy as np
from numpy import linalg as LA
from numpy import inf
import matplotlib.pyplot as plt
from copy import deepcopy
import random as rand
import sys
from random import randint
    # Sys used for error exit code
import sys
    # os used for generating gif
import os


def f(X,t):
    [a,b,c,k,gamma]=X
    return (a*t**2.0+b*t+c)*sin(k*t+gamma)


def partiala(X,t):
    [a,b,c,k,gamma]=X
    return -1.0*(t**2.0)*sin(k*t+gamma)

def partialb(X,t):
    [a,b,c,k,gamma]=X
    return -1.0*t*sin(k*t+gamma)

def partialc(X,t):
    [a,b,c,k,gamma]=X
    return -1.0*sin(k*t+gamma)

def partialk(X,t):
    [a,b,c,k,gamma]=X
    return -1.0*cos(k*t+gamma)*(a*t**2.0+b*t+c)*t

def partialgamma(X,t):
    [a,b,c,k,gamma]=X
    return -1.0*cos(k*t+gamma)*(a*t**2.0+b*t+c)


def createJacobian(X,tvect):
    J = np.zeros((len(tvect),len(X)))
    for i in range(len(tvect)):
        t = tvect[i]
        J[i,0] = partiala(X,t)
        J[i,1] = partialb(X,t)
        J[i,2] = partialc(X,t)
        J[i,3] = partialk(X,t)
        J[i,4] = partialgamma(X,t)
    return J

def createResidual(y,X,tvect):
    a = X[0]
    b = X[1]
    c = X[2]
    k = X[3]
    gamma = X[4]
    resid  = np.zeros(len(tvect))
    for i in range(len(tvect)):
        t = tvect[i]
        resid[i] = y[i]-f(X,t)
#        print resid[i]
    return resid

def NewtonIterate(y,X,tvect):
    flag = 0
    J = createJacobian(X,tvect)
    resid = createResidual(y,X,tvect)
    #print "resid", resid
    error = 1
    maxiters = 500
    iters = 0
    tol=1e-6
    while(error >tol and iters<maxiters):
        iters +=1
        RHS = -1.0*np.dot(np.transpose(J),resid)
        JTJ = np.dot(np.transpose(J),J)
        s = LA.solve(JTJ,RHS)
        X = X+s
        J = createJacobian(X,tvect)
        resid = createResidual(y,X,tvect)
        error = LA.norm(s,2)
    if (iters==maxiters):
        print "    ========="
        print "    Maxed out"
        print "    ========="
        flag = 1
    else:
        print "Newtons method completed in ", iters, " iterations."
    return X, flag

def getOmega(y,t):
    tmat=np.ones((t.shape[0],t.shape[1]+1));
    tmat[:,:-1]=t
    LHS=np.dot(np.transpose(tmat),tmat)
    RHS=np.dot(np.transpose(tmat),y)
    x= LA.solve(LHS,RHS)
    [omg,phi]=x
    print "Omega: ", omg
    return omg, phi

def genT(nsets,T,t0):
    t=np.zeros((nsets,1))
    for i in range(len(t)):
        t[i]=i*(T-t0)/nsets;
    #print "t:", t
    return t;

def solveForX(X,nsets,npts,data,omgS,xinit,pltOrig,vert):
    for i in range(nsets):
        print "\nSet #",i+1
        tvect, y= origFunc(data, npts,i)
        X, flag = NewtonIterate(y,X,tvect)
        # Plot data if desired
        if (pltOrig!=0):
            plotOrig(tvect,y,i,vert)
        # Flag is used to determine if newton's method maxed out
        if (flag!=0):
            print "X: ", xinit
            sys.exit("Poor initial guess")
        # Record the returned omg to use for later
        omgS[i]=X[-1];
        print "X: ", X
    return X

def plotOrig(tvect,y,i,vert):
        if (vert!=0):
            plt.plot(tvect,y)
            plt.axis([0, 12, -85, 80])
        else:
            plt.plot(y,12-tvect)
            plt.axis([-85, 80, 0, 12])
        plt.title("Plot of the original data. t="+ str("%.1f" %(i*0.1,)))
        figName='origData'+str("%1d" %(i,))+'.png'
        plt.savefig(figName)
        #plt.show()
        plt.clf()

def origFunc(data, npts, i):
    tvect = data[i*npts:(i+1)*npts,[0]]
    y = data[i*npts:(i+1)*npts,[1]]
    return tvect, y

def simFunc(xpts, tpt, X):
    [a,b,c,k,omg]=X
    return (a*xpts**2.0+b*xpts+c)*np.sin(k*xpts+omg*tpt)

def plotSwim(X,t,npts, pltOrig, vert,cleanup, start=0,end=12):
    xpts=np.linspace(start,end,npts)
    for i in range(len(t)):
        tpt=t[i]+t[1]
        ypts=simFunc(xpts,tpt,X)
        if (vert!=0):
            plt.plot(xpts,ypts)
            plt.axis([0, 12, -85, 80])
        else:
            plt.plot(ypts,12-xpts)
            plt.axis([-85, 80,0, 12])
        plt.title("Plot of the simulated data. t="+ str("%.1f" %(tpt-t[1],)))
        figName='simData'+str("%1d" %((10*tpt-t[1]),))+'.png'
        plt.savefig(figName)
        #plt.show()
        plt.clf()
    genSimAnimation(cleanup)
    if (pltOrig!=0):
        genOrigAnimation(cleanup)

def genSimAnimation(cleanup):
    print "Generating simulation animation"
    os.system("convert -delay 12 -dispose Background +page simData*.png -loop 0 simData.gif")
    if (cleanup!=0):
        os.system("rm simData*.png")
    os.system("xdg-open simData.gif")

def genOrigAnimation(cleanup):
    print "Generating original animation"
    os.system("convert -delay 12 -dispose Background +page origData*.png -loop 0 origData.gif")
    if (cleanup!=0):
        os.system("rm origData*.png")
    os.system("xdg-open origData.gif")

def swapCols(data):
    # This function swaps the x and y values if the data is for 
    #  a vertical swimmer
    temp = np.copy(data[:, 0])
    data[:, 0] = data[:, 1]
    data[:, 1] = temp
    return data
