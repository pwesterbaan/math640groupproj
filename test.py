# Define standard library imports.
from math import*
import numpy as np
from numpy import linalg as LA
from numpy import inf
import matplotlib.pyplot as plt
from copy import deepcopy
import random as rand
import sys
from random import randint
from functions import *
from random import randint
    # Sys used for error exit code
import sys
    # os used for generating gif
import os

#os.system("rm simData*.png")
#os.system("rm origData*.png")

# Flags
pltOrig = 1   # 0 to turn off original plot
pltSim  = 1   # 0 to turn off both plots
cleanup = 1   # 0 to keep pictures
vert    = 1   # 0 to rotate to plot vertical swimmer
vertInp = 1   # 0 to take in vertical input


# Opening data file
with open('ourdata.dat','r') as f:
    rows=[map(float,L.strip().split(' ')) for L in f]  # list of lists
data=np.array(rows)

if (vertInp==0):
    data=swapCols(data)

# Breakdown dependent on given data
nsets = 10
npts  = len(data)/nsets

# Initial guess ranges
#np.random.seed(250)
a,b,c,k,g = [1.0,1.0,1.0,1.0,1.0 ]
T=1.0; t0=0.0; wid = 0.3
X = [np.random.uniform(a-wid,a+wid),
     np.random.uniform(b-wid,b+wid),
     np.random.uniform(c-wid,c+wid),
     np.random.uniform(k-wid,k+wid),
     np.random.uniform(g-wid,g+wid)]
xinit=X
print "Initial guess:", X

# omgS is used to store all the returned values for X. Used to find omg!
omgS=np.zeros(nsets)

X=solveForX(X,nsets,npts,data,omgS,xinit, pltOrig,vert)
t=genT(nsets,T,t0)
omg, phi=getOmega(omgS,t)
# Place the correct omega into the X vector
X[-1]=omg
print "\n[a, b, c, k, omega]:", X
# Plot and animate the simulation
if (pltSim!=0):
    plotSwim(X,t,npts,pltOrig,vert,cleanup)
