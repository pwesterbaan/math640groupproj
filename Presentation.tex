\documentclass{beamer}
\usepackage{times}
\usepackage{amsmath, mathrsfs, amssymb, xfrac, amsthm}
\newcommand{\Real}{\mathbb R}
\usepackage{multirow, pbox, hhline}
\newcommand{\Lap}{\mathscr{L}}
%\beamerdefaultoverlayspecification{<+->}
%\usetheme{Darmstadt}
%\usepackage{upgreek}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
%\usepackage{textcomp}
\usebackgroundtemplate%
{
\includegraphics{bkGrnd}%
}
\title{Numerical Swimming Simulations of Organisms using Sinusoidal Undulation}
\author{Nathan Lilla, Michael J. McLaughlin, Suzanne Smedberg, Peter Westerbaan}
\date{December 11, 2015}

\begin{document}
\maketitle
\begin{frame}
Modeling biological motion presents a complex problem. Even a simple organism such as a eukaryotic flagellar swimmer in a fluid presents a unique set of challenges.
\begin{itemize}
\item
Macro organisms like the tuna can swim approximately 10 body lengths a second.
\item 
Their microscopic counterparts can move more with relative speeds of an order of larger magnitude.
\item
Larger animals can control the undulating motion in both homogeneous fluids and those of varying densities to achieve different locomotive goals.  
\end{itemize}
While they can be so different in structure, relative speed, and biological outcome, all of these swimming bodies follow the same simple swimmer model. 
\end{frame}

\begin{frame}
The simple swimmer that our project was tasked with was the \textit{Caenorhabditis elegans} nematode. In her research at Tulane University, Chia Yu created a simulation to model the interaction of the Lamprey in a fluid structure.\\ Our goal was to fit her results to a mathematical model in the form:
$$ f\left(x,\hat{t}\right) = \left(ax^2 + bx + c\right) \sin{\left(kx + \omega t\right)} $$
Non-linear data fitting will produce the values of $a,b,c,d,k$, and $\omega$ that allow for the best linear fit.
\end{frame}

\begin{frame}
\begin{center}
Newton's Method
\end{center}
As we learned in class, Newton's method is an effective way to solve non-linear equations. Recall the truncated Taylor series:
$$ \mathbf{f}\left(\mathbf{x} + \mathbf{s}\right) \approx \mathbf{f}\left(\mathbf{x}\right) + \mathbf{J_{f}}\left(\mathbf{x}\right)\mathbf{s}$$ 
where $\mathbf{J_{f}}\left(\mathbf{x}\right)$ denotes the Jacobian matrix of $\mathbf{x}$.  The Jacobian is defined by 
$$\mathbf{J_{f}}\left(\mathbf{x}\right)_{i,j} = \frac{\partial f_i\left(\mathbf{x}\right)}{\partial x_j}. $$
\end{frame}

\begin{frame}
Given that $\textbf{s = x}_n-\textbf{x}$, where $\textbf{x}_n$ is an approximate zero to $\textbf{f}$, we will have the following algorithm for Newton's Method:\\
$\,$\\
While $||\textbf{s}|| > $ tolerance,
\begin{itemize}
\item 
Solve $\textbf{J}_f(\textbf{x}_{n-1})\textbf{s}_n = -\textbf{f}(\textbf{x}_{n-1})$ for $\textbf{s}_n$. 
\item
Update $\textbf{x}_n = \textbf{x}_{n-1} + \textbf{s}_n$
\end{itemize}
\end{frame}

\begin{frame}
The previous algorithm only considers functions within the same dimension, where as our system is over-determined so we need to some adjustments. We can generalize Newton's algorithm considering the residuals. Our goal is to minimize the residual \textbf{r} which is
$$r_i(\mathbf{x}) = y_i - f\left(t_i,\mathbf{x}\right), \; \mbox{ for } \; i\in \{1,2,...,m\}. $$
So the goal becomes to minimize
$$\phi(\mathbf{x}) = \frac{1}{2} \|\mathbf{r}\|_{2}^{2} = \frac{1}{2}\mathbf{r}(\mathbf{x})^{T}\mathbf{r}(\mathbf{x}). $$
\end{frame}

\begin{frame}
Since we had an over-determined system, our problem will now looks like the method of least squares because we are minimizing the squares of the residuals. Our new algorithm for Newton's Method becomes:\\
$\,$\\
While $||\textbf{s}|| > $ tolerance,
\begin{itemize}
\item
Solve $\textbf{J}^T(\textbf{x}_{n-1})\textbf{J}(\textbf{x}_{n-1})\textbf{s}_n = -\textbf{J}^T(\textbf{x}_{n-1})\textbf{r}(\textbf{x}_{n-1})$ for $\textbf{s}_n$. 
\item
Update $\textbf{x}_n = \textbf{x}_{n-1} + \textbf{s}_n$
\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
Assumptions
\end{center}
\begin{enumerate}
\item We assume the swimmer is oriented in such a way that his motion is only in the positive $x$-direction.  An actual swimmer does not only swim in a straight line, but the considering the two-dimension motion adds a high level of complexity.
\item We assume that all the motion is relative only to time and horizontal position, and follows the same model.  
\item We assume that the dependence on time is linearly related to a variable $\phi$
\end{enumerate}
\end{frame}

\begin{frame}
Reconsider the swimmer model:
$$ f\left(\mathbf{x},\hat{t}\right) = \left(ax^2 + bx + c\right) \sin{\left(kx + \omega t\right)}. $$
To simplify the non-linear dependence of the sine function, we can consider a variable $\phi$ such that $\phi = \omega t$.  By replacement, then
$$ f\left(\mathbf{x},\hat{t}\right) = \left(ax^2 + bx + c\right) \sin{\left(kx + \phi\right)}, $$
for a vector $\hat{\mathbf{x}} = \left(a,b,c,k,\phi\right)^{T}$
\end{frame}

\begin{frame}
As our goal of least squares is to minimize the residual function, we consider the Jacobean terms:
\begin{eqnarray*}
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,1} & = & -t_i^2 \sin{\left(kt_i + \phi\right)} \\
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,2}  & = & -t_i \sin{\left(kt_i + \phi\right)} \\
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,3} & = & - \sin{\left(kt_i + \phi\right)} \\
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,4} & = & -k\left(at_i^2 + bt_i + c\right) \cos\left(kt_i + \phi\right) \\
\mathbf{J}\left(\hat{\mathbf{x}}\right)_{i,5} & = & -\left (at_i^2 + bt_i + c\right) \cos{\left(kt_i + \phi\right)} 
\end{eqnarray*}
\end{frame}

\begin{frame}
After the normal equations are set up, we have a $5 \times 5$ matrix system on which we iterate the points until the we have dropped below our tolerance and now have the values of $a,b,c,d,k,$ and $\phi$. However, we still need the value for $\omega$ which is time dependent. 
\end{frame}

\begin{frame}
Recall that $\phi = \omega t$, so for many values of $t$, the relationship between $\phi$ and $\omega$ can be determined by finding the slope of a linear fit of $\phi \,vs \,t$. \\
\begin{center}
\includegraphics[scale=0.6]{slope.png}
\end{center}

\end{frame}

\end{document}
